# ipprotocols
Repository for IP Protocols web extension files


FOR INITIAL SETUP OF ADD-ON, PLEASE SEE HOWTO.TXT

LORS DE LA PREMIERE UTILISATION DE L'ADD-ON, MERCI DE LIRE LE HOWTO.TXT

https://github.com/jisse44/ipprotocols/blob/master/HOWTO.txt

-------------------------------------
/########################################################################################################################

https://addons.mozilla.org/en-US/firefox/addon/ipprotocols/

Converts IP addresses into clickable links for use with VNC, SSH, Ping, Telnet...


IpProtocols recognizes IP addresses on web pages and converts them to clickable links.

IP address is not detected? Text can be selected and handled as an IP address and application can be launch by selecting the corresponding context menu item.

Applications are: VNC, FreeRDP or Rdesktop (TSE), Ping, SSH, Telnet and Nmap

See below for OS informations.

/***************************************************************************************************************
           You need to have (for defaults):

- For LINUX: - any terminal (gnome-terminal is used in the example script), vncviewer, rdesktop or freerdp, telnet, ping and nmap

- For Windows: 
	- UltraVNC : http://sourceforge.net/projects/ultravnc/ or http://www.uvnc.com/downloads/ultravnc.html
	- Putty: http://www.chiark.greenend.org.uk/~sgtatham/putty/
	- Nmap: http://nmap.org/ and must install WinPcap: http://www.winpcap.org/ (and start the service)

	/!\/!\ PLEASE don't abuse NMAP on public IP /!\/!\

***************************************************************************************************************/

This module is under GPL 2 licence. If you have any suggestion or contribution, please contact me, we can work together.

JC Prin
contact: jc85 (at) free fr


/###########################################################################################################################

https://addons.mozilla.org/fr/firefox/addon/ipprotocols/

Lancez VNC, Ping, Telnet SSH.. simplement en cliquant sur une adresse IP

IpProtocols est un module permettant de rendre cliquables les adresses IP présentes dans une page afin de lancer une application.
L'adresse IP n'est pas détectée? Pas de souci, sélectionnez-la et un clic droit fera apparaitre un menu contextuel permettant de lancer les applications.
Les protocoles supportés sont: VNC, Rdesktop (TSE), Ping, SSH, Telnet et Nmap

Fonctionnne sous Windows et Linux, sous réserve d'avoir installé les programmes correspondants (voir ci-dessous).

/***************************************************************************************************************
	Vous avez besoin (options par défaut):

- Pour LINUX: - un terminal (gnome-terminal est utilisé dans le script d'exemple), vncviewer, rdesktop ou freerdp, telnet, ping and nmap


- Pour Windows: 
	- UltraVNC : http://sourceforge.net/projects/ultravnc/ ou http://www.uvnc.com/downloads/ultravnc.html
	- Putty: http://www.chiark.greenend.org.uk/~sgtatham/putty/
	- Nmap: http://nmap.org/ et vous devez installerl WinPcap: http://www.winpcap.org/ (et démarrer le service)


	/!\/!\ SVP ne pas abuser de NMAP sur les IP publiques /!\/!\

***************************************************************************************************************/

Ce module est sous licence GPL 2. Si vous avez des modifications ou des suggestions, merci de m'en faire part, 
je ne manquerais pas de les incorporer.

JC Prin
contact: jc85 (at) free fr


