
You need to register the protocols 'ext+ipprotocols-XXX' to make the extension working.

In the file 'ipprotocols-handlers.reg' given here, you need to change the values of
the '[HKEY_CLASSES_ROOT\ext+ipprotocols-XXX\shell\open\command]' to point to your local script,
or you can create the directory 'c:\ipprotocols' and place the script 'ipprotocols.bat' into.

You MUST keep the \"%1\" value.


For the 'ipprotocols.bat' given as example, you need to install these programs:
- UltraVNC : http://sourceforge.net/projects/ultravnc/ or http://www.uvnc.com/downloads/ultravnc.html
- Putty: http://www.chiark.greenend.org.uk/~sgtatham/putty/
- Nmap: http://nmap.org/ (and must install WinPcap)
