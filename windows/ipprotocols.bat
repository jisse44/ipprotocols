@echo off & setlocal

REM ###########  DISCLAIMER
REM # This script is part of IPProtocols add-on
REM # This file can be found here: https://github.com/jisse44/ipprotocols
REM # This script is given as an example. You can make your own script with or without using it.
REM # Use this script at your own risk. The author takes no responsibility for any damages or otherwise using this script.
REM ###########

REM ########### BEGIN
REM #
REM ## example of args received from IPProtocols add-on for RDP command
REM # FFxArg="ext+ipprotocols-rdp://192.168.0.254/"

REM ###### SET commands programs. Those given below are examples.
REM # The XXX_COMMAND is immediately followed by the ip address in script
set VNC_COMMAND="c:\Program Files\uvnc bvba\UltraVNC\vncviewer.exe"
set RDP_COMMAND="c:\windows\system32\mstsc.exe"
set SSH_COMMAND="c:\Program Files (x86)\PuTTY\putty.exe"
set TELNET_COMMAND="C:\Windows\System32\telnet.exe"
set PING_COMMAND="c:\windows\system32\ping.exe"
set NMAP_COMMAND="c:\windows\system32\cmd.exe" /k "c:\Program Files (x86)\Nmap\nmap.exe"
set CUSTOM1_COMMAND=""
set CUSTOM2_COMMAND=""
REM ----------------------------------------------


REM ## Parse input to protocol and IP
set FFxArg=%1

For /F "tokens=1* delims=-" %%A IN (%FFxArg%) DO set FFxArg2="%%B"

For /F "tokens=1* delims=:" %%A IN (%FFxArg2%) DO (
	set protocol=%%A
    set FFxArg3="%%B"
)

For /F "tokens=1* delims=/" %%A IN (%FFxArg3%) DO set ip=%%A

REM @echo %protocol% >> c:\ipprotocols\ipprotocols.log
REM @echo %ip% >> c:\ipprotocols\ipprotocols.log

if "%protocol%" == "vnc" (
	REM @echo VNC >> c:\ipprotocols\ipprotocols.log
	Start "VNC" %VNC_COMMAND% %ip%
)
if "%protocol%" == "rdp" (
	REM @echo RDP >> c:\ipprotocols\ipprotocols.log
	Start "RDP" %RDP_COMMAND% /v:%ip%
)
if "%protocol%" == "ssh" (
	REM @echo SSH >> c:\ipprotocols\ipprotocols.log
	Start "SSH" %SSH_COMMAND% -ssh %ip%
)
if "%protocol%" == "telnet" (
	REM @echo TELNET >> c:\ipprotocols\ipprotocols.log
	start "Telnet" %TELNET_COMMAND% %ip%
)
if "%protocol%" == "ping" (
	REM @echo PING >> c:\ipprotocols\ipprotocols.log
	Start "Ping" %PING_COMMAND% -t %ip%
)
if "%protocol%" == "nmap" (
	REM @echo NMAP >> c:\ipprotocols\ipprotocols.log
	Start "NMAP" %NMAP_COMMAND% %ip%
)
if "%protocol%" == "custom1" (
	REM @echo CUSTOM1 >> c:\ipprotocols\ipprotocols.log
	Start "CUSTOM1" %CUSTOM1_COMMAND% %ip%
)
if "%protocol%" == "custom2" (
	REM @echo CUSTOM2 >> c:\ipprotocols\ipprotocols.log
	Start "CUSTOM2" %CUSTOM2_COMMAND% %ip%
)

:exit


REM #
REM ########### END OF FILE
