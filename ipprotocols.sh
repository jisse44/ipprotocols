#!/bin/bash

###########  DISCLAIMER
# This script is part of IPProtocols add-on
# This file can be found here: https://github.com/jisse44/ipprotocols
# This script is given as an example. You can make your own script with or without using it.
# Use this script at your own risk. The author takes no responsibility for any damages or otherwise using this script.
###########

########### BEGIN
#
##example of args received from IPProtocols add-on for RDP command
#ipprotocol="ext+ipprotocols-rdp://192.168.0.254"

###### SET commands programs. Those given below are examples.
#The XXX_COMMAND is immediately followed by the ip address in script
VNC_COMMAND="vncviewer -geometry 1820x980+10+50"
RDP_COMMAND="rdesktop -g 1820x980"
SSH_COMMAND="gnome-terminal -x ssh"
TELNET_COMMAND="gnome-terminal -x telnet"
PING_COMMAND="gnome-terminal -x ping -c 10"
NMAP_COMMAND="xterm -rv -hold -e nmap -Pn"
CUSTOM1_COMMAND="nemo"
CUSTOM2_COMMAND="xterm -rv -hold -e dig"
#####

#set to true to enable debugging of script (output in /tmp/ipprotocols.log)
DEBUG=false


## Parse input to protocol and IP
ipprotocol="$1"
protocol="${ipprotocol%://*}"
protocol="${protocol#*-}"
ip="${ipprotocol#*://}"


if $DEBUG; then
	echo "Protocol: "$protocol >> /tmp/ipprotocols.log 2>&1
	echo "IP: "$ip >> /tmp/ipprotocols.log 2>&1
fi


case "$protocol" in
	vnc)
		{
			${VNC_COMMAND} ${ip}
		} || {
			if $DEBUG; then
				echo "vnc protocol error: "$VNC_COMMAND >> /tmp/ipprotocols.log 2>&1
			fi
		}
	;;

	rdp)
		{
			${RDP_COMMAND} ${ip}
		} || {
			if $DEBUG; then
				echo "rdp protocol error: "$RDP_COMMAND >> /tmp/ipprotocols.log 2>&1
			fi
		}
	;;

	ssh)
		{
			${SSH_COMMAND} ${ip}
		} || {
			if $DEBUG; then
				echo "ssh protocol error: "$SSH_COMMAND >> /tmp/ipprotocols.log 2>&1
			fi
		}
	;;

	telnet)
		{
			${TELNET_COMMAND} ${ip}
		} || {
			if $DEBUG; then
				echo "telnet protocol error: "$TELNET_COMMAND >> /tmp/ipprotocols.log 2>&1
			fi
		}
	;;

	ping)
		{
			${PING_COMMAND} ${ip}
		} || {
			if [[ "DEBUG" == "true" ]]; then
				echo "ping protocol error: "$PING_COMMAND >> /tmp/ipprotocols.log 2>&1
			fi
		}		
	;;

	nmap)
		{
			${NMAP_COMMAND} ${ip}
		} || {
			if $DEBUG; then
				echo "nmap protocol error: "$NMAP_COMMAND >> /tmp/ipprotocols.log 2>&1
			fi
		}		
	;;

	custom1)
		{
			${CUSTOM1_COMMAND} "smb://administrator@"${ip}"/c$"
		} || {
			if $DEBUG; then
				echo "custom1 protocol error: "$CUSTOM1_COMMAND >> /tmp/ipprotocols.log 2>&1
			fi
		}		
	;;

	custom2)
		{
			${CUSTOM2_COMMAND} ${ip}
		} || {
			if $DEBUG; then
				echo "custom2 protocol error: "$CUSTOM2_COMMAND >> /tmp/ipprotocols.log 2>&1
			fi
		}		
	;;

        *)
		if $DEBUG; then
			echo "Error: unknown protocol '"$protocol"'" >> /tmp/ipprotocols.log 2>&1
		fi
		exit 1
 
esac

#
########### END OF FILE
